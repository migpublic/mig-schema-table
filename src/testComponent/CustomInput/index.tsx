import { ITdBodyProps } from "../../component";
import React from "react";
import { IExampleData } from "../../exampleData.ts";

const CustomInput = React.memo(
  ({
    rowData,
    rowIndex,
    onChange,
    selectedindex,
  }: ITdBodyProps<IExampleData> & {
    onChange?: () => void;
    selectedindex?: number;
  }) => {
    if (selectedindex === undefined || selectedindex !== rowIndex) {
      return <b>{rowData?.url}</b>;
    }
    return <input value={rowData?.url} onChange={onChange} />;
  },
);

export default CustomInput;
