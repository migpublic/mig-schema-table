export const DEFAULT_DATE_FORMAT = "dd MMM yyyy";
export const DEFAULT_DATE_TIME_FORMAT = "dd MMM yyyy HH:mm";
export const MINIMUM_COLUMN_WIDTH = 25;
export const RESIZER_WIDTH = 3;

export enum ENumberColumnFilterOperation {
  "GT" = "GT",
  "LT" = "LT",
  "EQ" = "EQ",
}

export enum EColumnFilterStatus {
  UNAVAILABLE = "UNAVAILABLE",
  AVAILABLE = "AVAILABLE",
  ACTIVE = "ACTIVE",
}
