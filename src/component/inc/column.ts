import { oas31 } from "openapi3-ts";
import { IColumnConfig } from "../types.ts";

export function getIsColumnSortable<T>(
  isTableSortable: boolean,
  propSchema: oas31.SchemaObject | undefined,
  propConfig: IColumnConfig<T> | undefined,
) {
  return !!(
    isTableSortable &&
    propConfig?.isSortable !== false &&
    (propSchema || propConfig?.renderData || propConfig?.sort)
  );
}
