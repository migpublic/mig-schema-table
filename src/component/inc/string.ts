import { unCamel } from "mig-data-tools";

const db: { [key: string]: string } = {
  "Europe/Amsterdam": "AMS",
  "Europe/Berlin": "AMS",
  "Asia/Jakarta": "JKT",
  "Asia/Bangkok": "JKT",
  showingFilteredCountOfTotalCount: "Showing {0} of {1}",
};

export function defaultTranslate(key: string, ...args: (string | number)[]) {
  let string = db[key] || unCamel(key);
  args.forEach((arg, index) => {
    string = string.replace(`{${index}}`, `${arg}`);
  });
  return string;
}
