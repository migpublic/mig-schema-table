import { format } from "date-fns";
import { nl } from "date-fns/locale";

export const localeFormat = (date: Date | number, dateFormat: string): string =>
  // replace jan. with jan
  format(date, dateFormat, { locale: nl }).replace(".", "");
