import React from "react";
import { SchemaTableContext } from "../provider/SchemaTableContext.tsx";

export function useIsAllRowsChecked() {
  const { checkedIndexes, disabledCheckedIndexes, sortedRenderData } =
    React.useContext(SchemaTableContext);

  return React.useMemo(() => {
    const tableData = [...(sortedRenderData || [])].filter(
      (el) =>
        (checkedIndexes ? checkedIndexes.includes(el._index) : true) ||
        (disabledCheckedIndexes
          ? !disabledCheckedIndexes.includes(el._index)
          : true),
    );
    return (
      checkedIndexes?.length !== 0 &&
      tableData.length === checkedIndexes?.length
    );
  }, [checkedIndexes, disabledCheckedIndexes, sortedRenderData]);
}
