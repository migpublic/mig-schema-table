import React from "react";
import { SchemaTableContext } from "../provider/SchemaTableContext.tsx";

export function useOnColumnsWidthsChange() {
  const { setCustomColumnWidths, settingsStorageKey } =
    React.useContext(SchemaTableContext);

  return React.useCallback(
    (newColumnWidths: number[]) => {
      if (settingsStorageKey) {
        localStorage.setItem(
          `${settingsStorageKey}.columnWidths`,
          JSON.stringify(newColumnWidths),
        );
      }
      setCustomColumnWidths(newColumnWidths);
    },
    [setCustomColumnWidths, settingsStorageKey],
  );
}
