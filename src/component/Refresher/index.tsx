import React from "react";
import { differenceInSeconds } from "date-fns";
import { localeFormat } from "../inc/date.ts";

interface IRefresherProps {
  refresh: () => void;
  period?: number;
}

const Refresher = React.memo(({ refresh, period = 60 }: IRefresherProps) => {
  const [currentDate, setCurrentDate] = React.useState(new Date());
  const [lastRefreshDate, setLastRefreshDate] = React.useState(new Date());

  const refreshPublicationIssues = React.useCallback(() => {
    setLastRefreshDate(new Date());
    refresh();
  }, [refresh]);

  const age = React.useMemo(
    () => differenceInSeconds(currentDate, lastRefreshDate),
    [currentDate, lastRefreshDate],
  );

  React.useEffect(() => {
    if (age > period) {
      refreshPublicationIssues();
    }
  }, [age, currentDate, lastRefreshDate, period, refreshPublicationIssues]);

  React.useEffect(() => {
    const incrementAge = () => {
      setCurrentDate(new Date());
    };
    const incrementAgeInterval = setInterval(incrementAge, 1_000);
    return () => {
      clearInterval(incrementAgeInterval);
    };
  }, []);

  const refreshIn = period - age;

  return (
    <div
      className="mx-auto"
      title={`Refresh ${refreshIn > 0 ? `in ${refreshIn} seconds` : "NOW"}`}
    >
      Last update {localeFormat(lastRefreshDate, "HH:mm")}
    </div>
  );
});

export default Refresher;
