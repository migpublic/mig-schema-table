import React from "react";
import { IFilterMenuComponentProps } from "./SchemaTable/ThMenu/FilterMenuComponent";

export interface ITdBodyProps<T> {
  rowData: T;
  dataIndex: number;
  rowIndex: number;
}

export interface IColumnConfig<T> {
  FilterMenu?: React.ComponentType<IFilterMenuComponentProps>;
  align?: "start" | "center" | "end";
  dateFormat?: string;
  defaultSortDesc?: boolean;
  filter?: (rowData: T, columnFilterValue: unknown) => boolean;
  hidden?: boolean;
  hoverTitle?: string;
  isFilterable?: boolean;
  order?: number;
  TdBody?: React.ComponentType<ITdBodyProps<T> & Record<string, unknown>>;
  tdBodyProps?: Record<string, unknown>;
  renderData?: (rowData: T, dataIndex: number) => string;
  showTimezones?: false;
  sort?: (a: T, b: T, sortAsc: boolean) => number;
  sortByValue?: boolean;
  isSortable?: boolean;
  title?: string | React.ReactElement;
  width?: number;
  timezone?: "Asia/Jakarta" | "Europe/Amsterdam";
}

export interface IRenderData {
  // @ts-expect-error TS can not handle this weird type?
  _index: number;
  [key: string]: string;
}
