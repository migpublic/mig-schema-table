import React from "react";
import { MINIMUM_COLUMN_WIDTH, RESIZER_WIDTH } from "../../inc/constant";
import { useOnColumnsWidthsChange } from "../../hooks/useOnColumnsWidthsChange.ts";

interface IColumnResizersProps {
  columnWidths: number[];
  resizeColumnIndex: number;
  setResizeColumnIndex: (newResizeColumnIndex: number) => void;
  tableBodyHeight: number;
}

const ColumnResizers = ({
  columnWidths,
  resizeColumnIndex,
  setResizeColumnIndex,
  tableBodyHeight,
}: IColumnResizersProps) => {
  const [dragStartX, setDragStartX] = React.useState(0);
  const onColumnWidthsChange = useOnColumnsWidthsChange();

  const setColumnDelta = React.useCallback(
    (columnDelta: number) => {
      const newColumnWidths = columnWidths.map(
        (columnWidth, columnWidthIndex) => {
          if (columnWidthIndex === resizeColumnIndex) {
            return Math.max(columnWidth + columnDelta, MINIMUM_COLUMN_WIDTH);
          }
          return columnWidth;
        },
      );
      onColumnWidthsChange(newColumnWidths);
    },
    [columnWidths, resizeColumnIndex, onColumnWidthsChange],
  );

  let pointer = 0;
  const onDragStart = React.useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      setResizeColumnIndex(parseInt(e.currentTarget.dataset.columnIndex!));
      setDragStartX(e.clientX);
    },
    [setResizeColumnIndex],
  );
  const onDragEnd = React.useCallback(
    (e: React.MouseEvent) => {
      setColumnDelta(e.clientX - dragStartX);
      setDragStartX(0);
      setResizeColumnIndex(-1);
    },
    [dragStartX, setColumnDelta, setResizeColumnIndex],
  );

  return (
    <div>
      {columnWidths.map((columnWidth, columnIndex) => {
        pointer += columnWidth;
        const classNames = ["mig-schema-table__column_resizer"];
        if (columnIndex === resizeColumnIndex) {
          classNames.push("mig-schema-table__column_resizer--dragged");
        }
        return (
          <div
            key={columnIndex}
            style={{ left: pointer - RESIZER_WIDTH, bottom: tableBodyHeight }}
            className={classNames.join(" ")}
            draggable="true"
            data-column-index={columnIndex}
            onDragStart={onDragStart}
            onDragEnd={onDragEnd}
          ></div>
        );
      })}
    </div>
  );
};

export default ColumnResizers;
