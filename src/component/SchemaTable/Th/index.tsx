import { oas31 } from "openapi3-ts";
import { SELECT_ALL_COLUMN_NAME } from "../constants";
import { EColumnFilterStatus } from "../../inc/constant.ts";
import React from "react";
import {
  ISchemaTableContext,
  SchemaTableContext,
} from "../../provider/SchemaTableContext.tsx";
import { useIsAllRowsChecked } from "../../hooks/useIsAllRowsChecked.ts";
import { getIsColumnSortable } from "../../inc/column.ts";
import { useOnColumnsWidthsChange } from "../../hooks/useOnColumnsWidthsChange.ts";
import { timeZone } from "mig-data-tools";

interface IThProps {
  index: number;
  style: React.CSSProperties;
}

const Th = <T,>({ index, style }: IThProps) => {
  const {
    checkedIndexes,
    columnNames,
    columnWidths,
    config,
    disabledCheckedIndexes,
    dropTargetIndex,
    isColumnFilterable,
    isSortable,
    setCheckedIndexes,
    properties,
    required,
    setCustomColumnNames,
    setDropTargetIndex,
    setFilterSortColumn,
    setMenuConfig,
    settingsStorageKey,
    tableDataState,
    sortedRenderData,
    translate,
    displayTimezone,
  } = React.useContext(SchemaTableContext) as ISchemaTableContext<T>;
  const [isDragging, setIsDragging] = React.useState(false);
  const isDropTarget = index === dropTargetIndex;
  const onColumnWidthsChange = useOnColumnsWidthsChange();
  const propName = columnNames[index];
  const propSchema = (
    propName === SELECT_ALL_COLUMN_NAME
      ? { type: "boolean" }
      : properties[propName]
  ) as oas31.SchemaObject | undefined;
  const thSortAsc =
    tableDataState.sortColumn === propName ? tableDataState.sortAsc : undefined;
  const propConfig = config ? config[propName] : undefined;
  let columnFilterStatus =
    isColumnFilterable &&
    (propSchema || propConfig?.FilterMenu) &&
    propConfig?.isFilterable !== false
      ? EColumnFilterStatus.AVAILABLE
      : EColumnFilterStatus.UNAVAILABLE;
  if (
    tableDataState.columnFilterMap &&
    tableDataState.columnFilterMap[propName] !== undefined
  ) {
    columnFilterStatus = EColumnFilterStatus.ACTIVE;
  }
  const isColumnSortable = getIsColumnSortable(
    isSortable,
    propSchema,
    propConfig,
  );

  const classNames = [
    `mig-schema-table__th`,
    `mig-schema-table__th--filter-${columnFilterStatus}`,
    `mig-schema-table__th--prop-${propName}`,
  ];
  classNames.push(
    isColumnSortable
      ? "mig-schema-table__th--sortable"
      : "mig-schema-table__th--unsortable",
  );
  if (thSortAsc !== undefined) {
    classNames.push("mig-schema-table__th--sorted");
  }
  if (isDragging) {
    classNames.push(`mig-schema-table__th--dragging`);
  }
  if (isDropTarget) {
    classNames.push(`mig-schema-table__th--drop-target`);
  }

  const { format } = propSchema || {};
  const {
    align,
    defaultSortDesc,
    showTimezones = !displayTimezone,
    title,
  } = propConfig || {};

  const onLabelClick = React.useCallback(() => {
    if (!isColumnSortable) {
      return;
    }
    if (thSortAsc === undefined) {
      setFilterSortColumn(propName, !defaultSortDesc);
      return;
    }
    setFilterSortColumn(propName, !thSortAsc);
  }, [
    isColumnSortable,
    defaultSortDesc,
    propName,
    setFilterSortColumn,
    thSortAsc,
  ]);
  const propIsRequired = required.includes(propName);
  const onTriggerClick = React.useCallback(
    (e: React.MouseEvent<HTMLElement>) => {
      const referenceElement = e.currentTarget;
      setMenuConfig((menuConfig) => {
        if (menuConfig?.propName === propName) {
          return undefined;
        }
        return {
          propConfig,
          propIsRequired,
          propName,
          referenceElement,
        };
      });
    },
    [propConfig, propIsRequired, propName, setMenuConfig],
  );

  const labelBody = React.useMemo(() => {
    if (title !== undefined) {
      return title;
    }

    if (format === "date-time" && showTimezones) {
      return `${translate(propName)} (${displayTimezone ? translate(displayTimezone) : translate(timeZone)})`;
    }

    return translate(propName);
  }, [title, format, showTimezones, translate, propName, displayTimezone]);

  const onColumnPositionChange = React.useCallback(
    (dragColumnName: string, dropColumnName: string) => {
      if (!columnNames || !columnWidths) {
        return;
      }

      // First move the actual name...
      const dragColumnIndex = columnNames.indexOf(dragColumnName);
      const newColumnNames = [...columnNames];
      newColumnNames.splice(dragColumnIndex, 1);
      const dropColumnIndex = newColumnNames.indexOf(dropColumnName);
      newColumnNames.splice(dropColumnIndex + 1, 0, dragColumnName);
      setCustomColumnNames(newColumnNames);

      // ...then make sure the width of column is moved as well
      const newColumnWidths = [...columnWidths];
      const draggedColumnWidth = newColumnWidths[dragColumnIndex];
      newColumnWidths.splice(dragColumnIndex, 1);
      newColumnWidths.splice(dropColumnIndex + 1, 0, draggedColumnWidth);
      onColumnWidthsChange(newColumnWidths);

      if (!settingsStorageKey) {
        return;
      }
      localStorage.setItem(
        `${settingsStorageKey}.columnNames`,
        JSON.stringify(newColumnNames),
      );
    },
    [
      columnNames,
      columnWidths,
      onColumnWidthsChange,
      settingsStorageKey,
      setCustomColumnNames,
    ],
  );

  const onDragStart = React.useCallback<React.DragEventHandler>(
    (e) => {
      if (!onColumnPositionChange) {
        return;
      }
      setIsDragging(true);
      e.dataTransfer.effectAllowed = "move";
      e.dataTransfer.dropEffect = "move";
      e.dataTransfer.setData("text/plain", propName);
    },
    [onColumnPositionChange, propName],
  );

  const onDragEnd = React.useCallback<React.DragEventHandler>(() => {
    if (!onColumnPositionChange) {
      return;
    }
    setIsDragging(false);
    setDropTargetIndex(-1);
  }, [onColumnPositionChange, setDropTargetIndex]);

  const onDragOver = React.useCallback<React.DragEventHandler>(
    (e) => {
      if (!onColumnPositionChange) {
        return;
      }
      e.preventDefault();
      setDropTargetIndex(index);
    },
    [index, onColumnPositionChange, setDropTargetIndex],
  );

  const onDrop = React.useCallback<React.DragEventHandler>(
    (e) => {
      setDropTargetIndex(-1);
      if (!onColumnPositionChange || !propName) {
        return;
      }
      const sourcePropName = e.dataTransfer.getData("text/plain");
      if (!sourcePropName || sourcePropName === propName) {
        return;
      }
      onColumnPositionChange(sourcePropName, propName);
    },
    [onColumnPositionChange, propName, setDropTargetIndex],
  );

  const isAllRowsChecked = useIsAllRowsChecked();
  const onSelectAllIndexesHandler = React.useCallback(() => {
    if (!setCheckedIndexes || !sortedRenderData) {
      return;
    }
    setCheckedIndexes(
      isAllRowsChecked
        ? []
        : sortedRenderData
            .map((el) => el._index)
            .filter((index) => !disabledCheckedIndexes?.includes(index)),
    );
  }, [
    setCheckedIndexes,
    sortedRenderData,
    isAllRowsChecked,
    disabledCheckedIndexes,
  ]);

  if (propName === SELECT_ALL_COLUMN_NAME) {
    return (
      <div style={style} className={classNames.join(" ")}>
        <div
          style={{
            width: "100%",
            textAlign: "center",
          }}
          title={`${checkedIndexes?.length || 0} selected`}
        >
          <input
            type="checkbox"
            name="selectAll"
            checked={isAllRowsChecked}
            onChange={onSelectAllIndexesHandler}
          />
        </div>
      </div>
    );
  }

  switch (propSchema?.type) {
    case "boolean":
      classNames.push(
        `text-${align || "center"}`,
        `justify-content-${align || "center"}`,
      );
      break;
    case "integer":
    case "number":
      classNames.push(
        `text-${align || "end"}`,
        `justify-content-${align || "end"}`,
      );
      break;
    default:
      if (align) {
        classNames.push(`text-${align}`);
      }
  }

  let hoverTitle = propConfig?.hoverTitle;
  if (!hoverTitle && typeof labelBody === "string") {
    hoverTitle = labelBody;
  }

  return (
    <div
      className={classNames.join(" ")}
      style={style}
      title={hoverTitle}
      draggable
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
      onDragOver={onDragOver}
      onDrop={onDrop}
    >
      <div
        className="mig-schema-table__th__label-body"
        style={{ lineHeight: "44px" }}
        onClick={onLabelClick}
      >
        <span className="mig-schema-table__th__label-body-text">
          {labelBody}
        </span>
        {thSortAsc === undefined ? null : (
          <span className="mig-schema-table__th__sort-icon">
            {thSortAsc ? "↓" : "↑"}
          </span>
        )}
      </div>
      {isColumnSortable ||
      columnFilterStatus !== EColumnFilterStatus.UNAVAILABLE ? (
        <button
          className="mig-schema-table__th__trigger-el"
          onClick={onTriggerClick}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="#404040"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <polyline points="6 9 12 15 18 9" />
          </svg>
        </button>
      ) : null}
    </div>
  );
};

const MemoizedTh = React.memo(Th) as typeof Th;

export default MemoizedTh;
