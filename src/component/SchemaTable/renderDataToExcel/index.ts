import {Workbook} from "exceljs";
import {difference} from "lodash";
import {saveAs} from "file-saver";
import {IRenderData} from "../../types.ts";

export const renderDataToExcel = (sortedRenderData: IRenderData[]) => {

    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet("Data");
    if (!sortedRenderData.length) {
        return;
    }
    worksheet.addRow(
        difference(Object.keys(sortedRenderData[0]), [
            "_index",
            "SELECT_ALL_COLUMN_NAME",
        ])
    );
    worksheet.getRow(1).font = { bold: true };
    worksheet.addRows(
        sortedRenderData.map((row) => {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const { _index, SELECT_ALL_COLUMN_NAME, ...data } = row;
            return Object.values(data);
        })
    );

    // Save the workbook to a Blob
    workbook.xlsx
        .writeBuffer()
        .then((buffer) => {
            const blob = new Blob([buffer], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            });
            saveAs(blob, "export.xlsx");
        })
        .catch((error) => {
            console.error("Error generating Excel file:", error);
        });


}
