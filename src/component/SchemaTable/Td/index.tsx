import React from "react";
import { oas31 } from "openapi3-ts";
import { SELECT_ALL_COLUMN_NAME } from "../constants";
import { DEFAULT_DATE_TIME_FORMAT } from "../../inc/constant";
import {
  ISchemaTableContext,
  SchemaTableContext,
} from "../../provider/SchemaTableContext.tsx";
import { range } from "lodash";
import { localeFormatInTimeZone, timeZone } from "mig-data-tools";

interface ITdProps {
  columnIndex: number;
  rowIndex: number;
  style: React.CSSProperties;
}

const Td = <T,>({ columnIndex, rowIndex, style }: ITdProps) => {
  const {
    checkedIndexes,
    columnNames,
    config,
    disabledCheckedIndexes,
    displayTimezone,
    getRowClassName,
    getRowSelected,
    isItemLoaded,
    lastCheckChangedRowIndex,
    onRowClick,
    onRowDoubleClick,
    properties,
    setCheckedIndexes,
    setLastCheckChangedRowIndex,
    sortedRenderData,
    sourceData,
    translate,
  } = React.useContext(SchemaTableContext) as ISchemaTableContext<T>;

  const propName: string | undefined = columnNames![columnIndex];
  const propSchema = (
    propName === SELECT_ALL_COLUMN_NAME
      ? { type: "boolean" }
      : properties[propName]
  ) as oas31.SchemaObject | undefined;
  const propConfig = config ? config[propName] : undefined;

  const onCheckedChange = React.useCallback(
    (rowIndex: number) => {
      if (!setCheckedIndexes) {
        return;
      }
      setCheckedIndexes((checkedIndexes) => {
        const wasChecked = checkedIndexes && checkedIndexes.includes(rowIndex);
        return wasChecked
          ? checkedIndexes.filter((index) => index !== rowIndex)
          : checkedIndexes
            ? [...checkedIndexes, rowIndex]
            : [rowIndex];
      });
    },
    [setCheckedIndexes],
  );

  const isLoaded = isItemLoaded(rowIndex);

  const onTdClick = React.useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      if (!sourceData || !sortedRenderData || !onRowClick || !isLoaded) {
        return;
      }
      const { rowIndex } = e.currentTarget.dataset;
      if (!rowIndex) {
        return;
      }
      const row = sortedRenderData[parseInt(rowIndex, 10)];
      onRowClick(sourceData[row._index], row._index, e);
    },
    [isLoaded, onRowClick, sortedRenderData, sourceData],
  );

  const onTdDoubleClick = React.useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      if (
        !sourceData ||
        !sortedRenderData ||
        !onRowDoubleClick ||
        !isItemLoaded
      ) {
        return;
      }
      const { rowIndex } = e.currentTarget.dataset;
      if (!rowIndex) {
        return;
      }
      const row = sortedRenderData[parseInt(rowIndex, 10)];
      onRowDoubleClick(sourceData[row._index], row._index, e);
    },
    [isItemLoaded, onRowDoubleClick, sortedRenderData, sourceData],
  );

  const row = sortedRenderData ? sortedRenderData[rowIndex] : undefined;
  const { showTimezones, TdBody } = propConfig || {};

  const alternativeTimeZone = React.useMemo(() => {
    // An explicitly configure timezone doesn't need alternatives
    if (displayTimezone) {
      return null;
    }
    return timeZone.startsWith("Europe/") ? "Asia/Jakarta" : "Europe/Amsterdam";
  }, [displayTimezone]);

  const tdDivProps = React.useMemo<
    React.HTMLAttributes<HTMLDivElement> | undefined
  >(() => {
    if (!row || !sortedRenderData) {
      return undefined;
    }

    let title = propName === SELECT_ALL_COLUMN_NAME ? undefined : row[propName];
    if (
      propSchema?.format &&
      propSchema.format === "date-time" &&
      showTimezones !== false
    ) {
      const originalData = sourceData ? sourceData[row._index] : undefined;
      const dateString: string | undefined = originalData
        ? ((originalData as never)[propName] as string)
        : undefined;
      const date = dateString ? new Date(dateString) : undefined;
      if (date && alternativeTimeZone) {
        title = `${localeFormatInTimeZone(
          date,
          alternativeTimeZone,
          DEFAULT_DATE_TIME_FORMAT,
        )} (${translate(alternativeTimeZone)})`;
      }
    }

    const classNames = [
      "mig-schema-table__td",
      `mig-schema-table__td--${rowIndex % 2 ? "odd" : "even"}`,
      `mig-schema-table__td--prop-${propName}`,
    ];

    if (
      sourceData &&
      getRowSelected &&
      getRowSelected(sourceData[row._index], row._index)
    ) {
      classNames.push("mig-schema-table__td--selected");
    }

    if (sourceData && getRowClassName) {
      classNames.push(
        getRowClassName(sourceData[row._index], row._index, sortedRenderData),
      );
    }

    return {
      "data-row-index": rowIndex,
      "data-column-index": columnIndex,
      style,
      className: classNames.join(" "),
      title,
    };
  }, [
    row,
    sortedRenderData,
    propName,
    propSchema?.format,
    showTimezones,
    rowIndex,
    sourceData,
    getRowSelected,
    getRowClassName,
    columnIndex,
    style,
    alternativeTimeZone,
    translate,
  ]);

  const onCheckboxMouseDown = React.useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      if (!onCheckedChange || !row) {
        return;
      }

      const targetRowIndex = parseInt(e.currentTarget.dataset.rowIndex!);
      if (e.shiftKey && lastCheckChangedRowIndex !== undefined) {
        const startRowIndex = checkedIndexes?.includes(lastCheckChangedRowIndex)
          ? lastCheckChangedRowIndex
          : undefined;
        if (startRowIndex !== undefined && sortedRenderData) {
          const endRowIndex = rowIndex;
          for (const toBeSelectedSortedRenderDataIndex of range(
            endRowIndex > startRowIndex ? startRowIndex + 1 : endRowIndex,
            endRowIndex > startRowIndex ? endRowIndex + 1 : startRowIndex,
          )) {
            const rowIndex =
              sortedRenderData[toBeSelectedSortedRenderDataIndex]._index;
            if (
              !disabledCheckedIndexes ||
              !disabledCheckedIndexes.includes(rowIndex)
            ) {
              onCheckedChange(rowIndex);
            }
          }
        }
      } else {
        onCheckedChange(row._index);
      }
      setLastCheckChangedRowIndex(targetRowIndex);
      checkboxRef.current?.focus();
    },
    [
      checkedIndexes,
      disabledCheckedIndexes,
      lastCheckChangedRowIndex,
      onCheckedChange,
      row,
      rowIndex,
      setLastCheckChangedRowIndex,
      sortedRenderData,
    ],
  );

  const checkboxRef = React.useRef<HTMLInputElement>(null);

  if (!row || !tdDivProps) {
    return null;
  }

  if (!isItemLoaded || (sourceData && !sourceData[row._index])) {
    return <div {...tdDivProps}>loading</div>;
  }

  if (propName === SELECT_ALL_COLUMN_NAME) {
    return (
      <div {...tdDivProps} onMouseDown={onCheckboxMouseDown}>
        <div style={{ textAlign: "center" }}>
          <input
            ref={checkboxRef}
            type="checkbox"
            readOnly /* change is handled by container to increase clickarea */
            checked={checkedIndexes?.includes(row._index)}
            disabled={disabledCheckedIndexes?.includes(row._index)}
          />
        </div>
      </div>
    );
  }

  tdDivProps.onClick = onTdClick;
  tdDivProps.onDoubleClick = onTdDoubleClick;

  if (TdBody && sourceData) {
    return (
      <div {...tdDivProps}>
        <TdBody
          dataIndex={row._index}
          rowData={sourceData[row._index]}
          rowIndex={rowIndex}
          {...propConfig?.tdBodyProps}
        />
      </div>
    );
  }

  const propValue = row[propName];

  switch (propSchema?.type) {
    case "boolean":
      tdDivProps.className += ` text-${propConfig?.align || "center"}`;
      break;
    case "number":
    case "integer":
      tdDivProps.className += ` text-${propConfig?.align || "end"}`;
      break;
    default:
      if (propConfig?.align) {
        tdDivProps.className += ` text-${propConfig.align}`;
      }
      if (propSchema?.format === "url" && propValue) {
        return (
          // @ts-expect-error investigate
          <a
            href={propValue}
            target="_blank"
            rel="noopener noreferrer"
            {...tdDivProps}
          >
            {propValue}
          </a>
        );
      }
  }
  return <div {...tdDivProps}>{propValue}</div>;
};

const MemoizedTd = React.memo(Td) as typeof Td;

export default MemoizedTd;
