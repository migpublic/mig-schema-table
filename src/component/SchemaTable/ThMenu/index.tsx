import React from "react";
import { oas31 } from "openapi3-ts";
import FilterMenuComponent from "./FilterMenuComponent";
import { IColumnConfig } from "../../types";
import { flip, useFloating } from "@floating-ui/react-dom";

export interface IThMenuConfig<T> {
  referenceElement: HTMLElement;
  propName: string;
  propConfig?: IColumnConfig<T>;
  propIsRequired: boolean;
}

type TThMenuProps<T> = IThMenuConfig<T> & {
  isSortable: boolean;
  isFilterable: boolean;
  onChange: (newValue: T | undefined, persistState?: boolean) => void;
  onClose: (e: MouseEvent | React.MouseEvent) => void;
  onInputKeyDown: (e: React.KeyboardEvent<HTMLElement>) => void;
  propIsRequired: boolean;
  propSchema: oas31.SchemaObject;
  setFilterSortColumn: (sortColumn: string, sortAsc: boolean) => void;
  translate: (key: string, ...args: Array<string | number>) => string;
  value: T | null;
};

const ThMenu = <T,>({
  isSortable,
  isFilterable,
  onChange,
  onClose,
  onInputKeyDown,
  propConfig,
  propIsRequired,
  propName,
  propSchema,
  referenceElement,
  setFilterSortColumn,
  translate,
  value,
}: TThMenuProps<T>) => {
  const menuFloating = useFloating({
    placement: "bottom-start",
    elements: {
      reference: referenceElement,
    },
    middleware: [flip()],
  });

  const subMenuFloating = useFloating({
    placement: "right-start",
    middleware: [flip()],
  });

  React.useEffect(() => {
    const onWindowClick = (e: MouseEvent) => {
      if (!menuFloating.elements.floating) {
        return;
      }

      let parent = e.target as null | ParentNode;
      while (parent && menuFloating.elements.floating) {
        if (
          parent === menuFloating.elements.floating ||
          parent === subMenuFloating.elements.floating
        ) {
          return;
        }
        parent = (
          parent.parentNode === window.document ? null : parent.parentNode
        ) as null | ParentNode;
      }
      onClose(e);
    };
    window.addEventListener("click", onWindowClick, { capture: true });
    return () => {
      window.removeEventListener("click", onWindowClick, { capture: true });
    };
  }, [
    menuFloating.elements.floating,
    onClose,
    subMenuFloating.elements.floating,
  ]);

  const FilterMenu = propConfig?.FilterMenu || FilterMenuComponent;

  const onSortAscendingClick = React.useCallback(
    (e: React.MouseEvent) => {
      setFilterSortColumn(propName, true);
      onClose(e);
    },
    [onClose, propName, setFilterSortColumn],
  );

  const onSortDescendingClick = React.useCallback(
    (e: React.MouseEvent) => {
      setFilterSortColumn(propName, false);
      onClose(e);
    },
    [onClose, propName, setFilterSortColumn],
  );

  const onFilterCheckboxChange = React.useCallback(() => {
    onChange(undefined, true);
  }, [onChange]);

  if (!isSortable && !isFilterable) {
    return null;
  }

  return (
    <>
      <div
        className="mig-schema-table mig-schema-table__th-menu"
        ref={menuFloating.refs.setFloating as never}
        style={menuFloating.floatingStyles}
      >
        <ol className="mig-schema-table-menu">
          {isSortable ? (
            <li onClick={onSortAscendingClick} style={{ padding: 8 }}>
              <span className="mig-schema-table__th-menu__icon">↓</span>{" "}
              {translate("sortAscending")}
            </li>
          ) : null}
          {isSortable ? (
            <li onClick={onSortDescendingClick} style={{ padding: 8 }}>
              <span className="mig-schema-table__th-menu__icon">↑</span>{" "}
              {translate("sortDescending")}
            </li>
          ) : null}
          {isFilterable ? (
            <li
              style={{ padding: 8 }}
              onMouseOver={(e) => {
                subMenuFloating.refs.setReference(e.currentTarget);
              }}
            >
              <span className="mig-schema-table__th-menu__icon">
                <input
                  type="checkbox"
                  id="mig-schema-table__th-menu__filters"
                  checked={value !== undefined}
                  disabled={value === undefined}
                  onChange={onFilterCheckboxChange}
                />
              </span>
              <label htmlFor="mig-schema-table__th-menu__filters">
                {translate("filters")}
              </label>
              <div className="mig-schema-table__th-menu__sub-menu-indicator"></div>
            </li>
          ) : null}
        </ol>
      </div>
      {subMenuFloating.elements.reference ? (
        <div
          className="mig-schema-table mig-schema-table__th-menu__sub-menu"
          ref={subMenuFloating.refs.setFloating}
          style={subMenuFloating.floatingStyles}
        >
          <FilterMenu
            columnFilterValue={value}
            onChange={onChange}
            onInputKeyDown={onInputKeyDown}
            propIsRequired={propIsRequired}
            propName={propName}
            propSchema={propSchema}
            translate={translate}
          />
        </div>
      ) : null}
    </>
  );
};

const MemoizedThMenu = React.memo(ThMenu) as typeof ThMenu;

export default MemoizedThMenu;
