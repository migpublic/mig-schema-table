import React from "react";
import { oas31 } from "openapi3-ts";
import {
  DEFAULT_DATE_FORMAT,
  DEFAULT_DATE_TIME_FORMAT,
  ENumberColumnFilterOperation,
} from "../../../inc/constant";
import DatePicker from "react-datepicker";
import { IDateColumnFilterValue, INumberColumnFilterValue } from "../../index";
import { Locale, endOfDay } from "date-fns";
import { SchemaTableContext } from "../../../provider/SchemaTableContext.tsx";
import { fromZonedTime, toZonedTime } from "date-fns-tz";
import { timeZone } from "mig-data-tools";
import { nl } from "date-fns/locale/nl";

const numberColumnFilterOperationsLabelMap = {
  [ENumberColumnFilterOperation.GT]: ">",
  [ENumberColumnFilterOperation.LT]: "<",
  [ENumberColumnFilterOperation.EQ]: "=",
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface IFilterMenuComponentProps<T = any> {
  columnFilterValue?: T;
  onChange: (newValue: T | undefined, persistState?: boolean) => void;
  onInputKeyDown: (e: React.KeyboardEvent<HTMLElement>) => void;
  propIsRequired: boolean;
  propName: string;
  propSchema?: oas31.SchemaObject;
  translate: (key: string, ...args: Array<string | number>) => string;
}
const FilterMenuComponent = React.memo(
  <T,>({
    columnFilterValue,
    onChange,
    onInputKeyDown,
    propIsRequired,
    propName,
    propSchema,
    translate,
  }: IFilterMenuComponentProps<T>) => {
    const { displayTimezone } = React.useContext(SchemaTableContext);
    const { type, format, minimum, maximum } = propSchema || {};
    const value = columnFilterValue;
    const isDateTime = format === "date-time";

    switch (type) {
      case "number":
      case "integer":
        const numberColumnFilterValue = (value ||
          {}) as INumberColumnFilterValue;
        const changeOperationValue = (
          operation: ">" | "<" | "=",
          el: HTMLInputElement,
          persistState: boolean,
        ) => {
          const newValue = {
            ...numberColumnFilterValue,
            [operation]: el.value === "" ? undefined : parseInt(el.value),
          };
          const filterValue = Object.values(newValue).find((opValue) =>
            isFinite(opValue as number),
          ) as number | undefined;
          onChange(
            filterValue !== undefined &&
              (minimum !== undefined
                ? (filterValue as number) >= minimum
                : true)
              ? (newValue as T)
              : undefined,
            persistState,
          );
        };

        return (
          <ol className="mig-schema-table-menu mig-schema-table__th-menu__filter-menu-component">
            {propIsRequired ? null : (
              <li style={{ padding: 8 }}>
                <label className="d-flex">
                  <input
                    type="checkbox"
                    style={{ marginRight: 14 }}
                    checked={!!numberColumnFilterValue.filterEmpty}
                    onChange={() => {
                      const { filterEmpty, ...newNumberColumnFilterValue } =
                        numberColumnFilterValue;
                      if (!filterEmpty) {
                        (
                          newNumberColumnFilterValue as IDateColumnFilterValue
                        ).filterEmpty = true;
                      }
                      onChange(
                        (Object.keys(newNumberColumnFilterValue).length
                          ? newNumberColumnFilterValue
                          : undefined) as T,
                        true,
                      );
                    }}
                  />
                  Hide empty values
                </label>
                <hr />
              </li>
            )}
            {Object.keys(ENumberColumnFilterOperation).map((operation) => {
              const val =
                numberColumnFilterValue[
                  operation as ENumberColumnFilterOperation
                ];
              return (
                <li key={operation}>
                  <label style={{ width: 40, paddingLeft: 16 }}>
                    {
                      numberColumnFilterOperationsLabelMap[
                        operation as ENumberColumnFilterOperation
                      ]
                    }
                  </label>
                  <input
                    className="form-control"
                    style={{ width: 120 }}
                    type="number"
                    value={val !== undefined ? val : ""}
                    data-prop-name={propName}
                    onChange={(e) => {
                      changeOperationValue(
                        operation as ">",
                        e.currentTarget,
                        false,
                      );
                    }}
                    onBlur={(e) => {
                      changeOperationValue(
                        operation as ">",
                        e.currentTarget,
                        true,
                      );
                    }}
                    onKeyDown={onInputKeyDown}
                    min={minimum}
                    max={maximum}
                  />
                </li>
              );
            })}
          </ol>
        );
      case "boolean":
        const optionValues = ["✓", "✕"];
        if (!propIsRequired) {
          optionValues.push("?");
        }
        let selectValue = value ? "✓" : "✕";
        if (value === null) {
          selectValue = "?";
        }
        if (value === undefined) {
          selectValue = "";
        }
        return (
          <ol className="mig-schema-table-menu mig-schema-table__th-menu__filter-menu-component">
            <li>
              <select
                autoFocus
                className="form-select"
                value={selectValue}
                data-prop-name={propName}
                onChange={(e) => {
                  switch (e.currentTarget.value) {
                    case "✓":
                      onChange(true as T, true);
                      break;

                    case "✕":
                      onChange(false as T, true);
                      break;

                    case "?":
                      onChange(null as T, true);
                      break;

                    default:
                      onChange(undefined, true);
                  }
                }}
              >
                <option key={"all"} value={""}>
                  All
                </option>
                {optionValues.map((optionValue: string) => (
                  <option
                    key={`column-filter-select-${optionValue}`}
                    value={optionValue}
                  >
                    {optionValue}
                  </option>
                ))}
              </select>
            </li>
          </ol>
        );

      // @ts-expect-error falls through
      case "string":
        if (propSchema?.enum) {
          return (
            <ol className="mig-schema-table-menu mig-schema-table__th-menu__filter-menu-component">
              <li>
                <select
                  autoFocus
                  className="form-select"
                  value={value as string}
                  data-prop-name={propName}
                  onChange={(e) => {
                    onChange((e.currentTarget.value || undefined) as T, true);
                  }}
                >
                  <option key={"all"} value={""}>
                    All
                  </option>
                  {propSchema.enum.map((value: string) => {
                    return (
                      <option
                        key={`column-filter-select-${value}`}
                        value={value}
                      >
                        {translate(value)}
                      </option>
                    );
                  })}
                </select>
              </li>
            </ol>
          );
        }
        if (isDateTime || format === "date") {
          const dateFormat = isDateTime
            ? DEFAULT_DATE_TIME_FORMAT
            : DEFAULT_DATE_FORMAT;
          const dateRangeValue = (columnFilterValue || {
            from: undefined,
            to: undefined,
            filterEmpty: undefined,
          }) as IDateColumnFilterValue;
          return (
            <ol className="mig-schema-table-menu mig-schema-table__th-menu__filter-menu-component">
              {propIsRequired ? null : (
                <li style={{ padding: 8 }}>
                  <label className="d-flex">
                    <input
                      type="checkbox"
                      checked={!!dateRangeValue.filterEmpty}
                      onChange={() => {
                        const { filterEmpty, ...newDateRangeValue } =
                          dateRangeValue;
                        if (!filterEmpty) {
                          (
                            newDateRangeValue as IDateColumnFilterValue
                          ).filterEmpty = true;
                        }
                        onChange(
                          Object.keys(newDateRangeValue).length
                            ? (newDateRangeValue as T)
                            : undefined,
                          true,
                        );
                      }}
                    />
                    Hide empty values
                  </label>
                  <hr />
                </li>
              )}
              <li style={{ padding: 8 }}>
                <label style={{ width: 120, paddingLeft: 4 }}>After</label>
                <DatePicker
                  dateFormat={dateFormat}
                  data-prop-name={propName}
                  locale={nl}
                  selected={
                    dateRangeValue.from
                      ? toZonedTime(
                          dateRangeValue.from,
                          displayTimezone || timeZone,
                        )
                      : null
                  }
                  onChange={(date: Date | null) => {
                    if (!date && !dateRangeValue.to) {
                      onChange(undefined, true);
                      return;
                    }

                    if (dateRangeValue.to && date && date > dateRangeValue.to) {
                      return;
                    }
                    const from = date
                      ? fromZonedTime(date, displayTimezone || timeZone)
                      : undefined;
                    onChange(
                      {
                        ...(columnFilterValue as object),
                        from,
                      } as T,
                      true,
                    );
                  }}
                  placeholderText={dateFormat}
                  isClearable
                  selectsStart
                  showTimeSelect={isDateTime}
                  showTimeInput={isDateTime}
                  showMonthDropdown
                  showYearDropdown
                  timeIntervals={15}
                  shouldCloseOnSelect={!isDateTime}
                />
              </li>
              <li style={{ padding: 8 }}>
                <label style={{ width: 120, paddingLeft: 4 }}>Before</label>
                <DatePicker
                  id={"filter-date"}
                  dateFormat={dateFormat}
                  data-prop-name={propName}
                  locale={nl as unknown as Locale}
                  selectsEnd
                  selected={
                    dateRangeValue.to
                      ? toZonedTime(
                          dateRangeValue.to,
                          displayTimezone || timeZone,
                        )
                      : null
                  }
                  showMonthDropdown
                  showYearDropdown
                  onChange={(date: Date | null) => {
                    if (!date && !dateRangeValue.from) {
                      onChange(undefined, true);
                      return;
                    }
                    const to = date
                      ? isDateTime
                        ? fromZonedTime(date, displayTimezone || timeZone)
                        : endOfDay(date)
                      : undefined;
                    if (dateRangeValue.from && to && to < dateRangeValue.from) {
                      return;
                    }
                    onChange(
                      {
                        ...(columnFilterValue as object),
                        to,
                      } as T,
                      true,
                    );
                  }}
                  placeholderText={dateFormat}
                  isClearable
                  startDate={dateRangeValue.from}
                  endDate={dateRangeValue.to}
                  showTimeInput={isDateTime}
                  showTimeSelect={isDateTime}
                  timeIntervals={15}
                  shouldCloseOnSelect={!isDateTime}
                />
              </li>
            </ol>
          );
        }
      // falls through

      default:
        return (
          <ol className="mig-schema-table-menu mig-schema-table__th-menu__filter-menu-component">
            <li>
              <input
                autoFocus
                type="search"
                className="form-control"
                placeholder={`Search ${propName}`}
                aria-label={`Search ${propName}`}
                value={(value || "") as string}
                data-prop-name={propName}
                onChange={(e) => {
                  onChange((e.currentTarget.value as T) || undefined, false);
                }}
                onKeyDown={onInputKeyDown}
                onBlur={(e) => {
                  onChange((e.currentTarget.value as T) || undefined, true);
                }}
              />
            </li>
          </ol>
        );
    }
  },
);

export default FilterMenuComponent;
