import React, { Dispatch, InputHTMLAttributes, SetStateAction } from "react";
import { createPortal } from "react-dom";
import { oas31 } from "openapi3-ts";
import {
  VariableSizeGrid,
  VariableSizeList,
  VariableSizeListProps,
} from "react-window";
import { localeFormat } from "../inc/date";
import { defaultTranslate } from "../inc/string";
import { IColumnConfig, IRenderData } from "../types";
import { SELECT_ALL_COLUMN_NAME, SELECT_ALL_COLUMN_WIDTH } from "./constants";
import Td from "./Td";
import {
  DEFAULT_DATE_FORMAT,
  DEFAULT_DATE_TIME_FORMAT,
  ENumberColumnFilterOperation,
  MINIMUM_COLUMN_WIDTH,
} from "../inc/constant";
import Th from "./Th";
import ThMenu, { IThMenuConfig } from "./ThMenu";
import ColumnResizers from "./ColumnResizers";
import { isFinite, sum } from "lodash";
import InfiniteLoader from "react-window-infinite-loader";
import { SchemaTableContext } from "../provider/SchemaTableContext.tsx";
import { getIsColumnSortable } from "../inc/column.ts";
import { isEqual } from "lodash";
import {
  localeFormatInTimeZone,
  parseLocationHash,
  serializeLocationHash,
} from "mig-data-tools";

const startOfTheWorldDate = new Date("1000-01-01 00:00:00Z");
const numberFormatter = new Intl.NumberFormat("nl-NL");
const currencyFormatter = new Intl.NumberFormat("nl-NL", {
  style: "currency",
  currency: "EUR",
});

export interface IColumnFilterMap {
  [propName: string]: unknown;
}

export interface ITableDataState {
  columnFilterMap: IColumnFilterMap;
  searchQuery: string;
  sortColumn?: string;
  sortAsc?: boolean;
}

export interface ICustomElementProps {
  renderData?: IRenderData[];
}

export interface ISchemaTableProps<T> {
  Heading?: React.ComponentType<
    VariableSizeListProps & {
      setFilterSortColumn?: (sortColumn: string, sortAsc: boolean) => void;
      sortAsc?: boolean;
      sortColumn?: string;
      sortedRenderData?: IRenderData[];
    }
  >;
  checkedIndexes?: number[];
  config?: { [propName: string]: IColumnConfig<T> };
  CustomSearchInput?: React.ComponentType<
    InputHTMLAttributes<HTMLInputElement>
  >;
  CustomElement?: React.ComponentType<ICustomElementProps>;
  customElementProps?: { [controlProp: string]: unknown };
  data: T[] | ((getDataProps: ITableDataState) => Promise<T[]>);
  defaultColumnFilters?: IColumnFilterMap;
  defaultSortAsc?: boolean;
  defaultSortColumn?: string;
  disabledCheckedIndexes?: number[];
  enableAutoFocus?: boolean;
  enableRowCounter?: boolean;
  getRowClassName?: (
    rowData: T,
    dataIndex: number,
    filteredSortedData: IRenderData[],
  ) => string;
  getRowSelected?: (rowData: T, dataIndex: number) => boolean;
  infiniteLoaderRef?: React.LegacyRef<InfiniteLoader>;
  isColumnFilterable?: boolean;
  isExportable?: boolean;
  isResizable?: boolean;
  isSearchable?: boolean;
  isSortable?: boolean;
  maxHeight?: number;
  setCheckedIndexes?: Dispatch<SetStateAction<number[]>>;
  onRowClick?: (rowData: T, dataIndex: number, event: React.MouseEvent) => void;
  onRowDoubleClick?: (
    rowData: T,
    dataIndex: number,
    event: React.MouseEvent,
  ) => void;
  onSearchEnter?: (searchQuery: string) => void;
  onTableDataStateChange?: (newTableDataState: ITableDataState) => void;
  rowHeight?: number;
  schema: oas31.SchemaObject;
  searchPlaceholder?: string;
  settingsStorageKey?: string;
  style?: React.CSSProperties;
  translate?: (key: string, ...args: Array<string | number>) => string;
  useFilterStateHash?: boolean;
  width?: number;
  variableSizeGridRef?: React.RefObject<VariableSizeGrid>;
  loadMoreItems?: (
    startIndex: number,
    stopIndex: number,
  ) => void | Promise<void>;
  itemCount?: number;
  displayTimezone?: "Europe/Amsterdam" | "Asia/Jakarta";
  autoRender?: boolean;
}

export interface IDateColumnFilterValue {
  from?: Date;
  to?: Date;
  filterEmpty?: boolean;
}

export interface INumberColumnFilterValue {
  [ENumberColumnFilterOperation.GT]: number;
  [ENumberColumnFilterOperation.LT]: number;
  [ENumberColumnFilterOperation.EQ]: number;
  filterEmpty?: boolean;
}

// export type TColumnFilterValue =
//   | string
//   | boolean
//   | number
//   | IDateColumnFilterValue
//   | INumberColumnFilterValue
//   | null;

function getSortByValue<T>(
  propSchema: oas31.SchemaObject | undefined,
  propConfig: IColumnConfig<T> | undefined,
): boolean {
  const { TdBody, sortByValue } = propConfig || {};
  if (sortByValue !== undefined) {
    return sortByValue;
  }
  if (!propSchema) {
    return false;
  }
  const { format, type } = propSchema;
  const isDate = format?.startsWith("date");
  return (
    isDate ||
    type === "boolean" ||
    type === "integer" ||
    type === "number" ||
    !!TdBody
  );
}

// Avoid using strict object type which conflicts with load more function
const SchemaTable = <T,>({
  Heading = VariableSizeList,
  checkedIndexes,
  config,
  CustomElement,
  customElementProps,
  data,
  defaultColumnFilters,
  defaultSortAsc = false,
  defaultSortColumn,
  disabledCheckedIndexes,
  enableAutoFocus = true,
  enableRowCounter = true,
  getRowClassName,
  getRowSelected,
  infiniteLoaderRef,
  isColumnFilterable = true,
  isExportable = true,
  isResizable = true,
  isSearchable = true,
  isSortable = true,
  maxHeight,
  setCheckedIndexes,
  onRowClick,
  onRowDoubleClick,
  onSearchEnter,
  onTableDataStateChange,
  rowHeight = 36,
  schema,
  searchPlaceholder,
  settingsStorageKey,
  style,
  translate = defaultTranslate,
  useFilterStateHash,
  width,
  variableSizeGridRef,
  loadMoreItems,
  itemCount,
  displayTimezone,
  autoRender,
  CustomSearchInput,
}: ISchemaTableProps<T>) => {
  const [customColumnNames, setCustomColumnNames] = React.useState<string[]>();
  const [customColumnWidths, setCustomColumnWidths] =
    React.useState<number[]>();
  const [resizeColumnIndex, setResizeColumnIndex] = React.useState(-1);
  const [dropTargetIndex, setDropTargetIndex] = React.useState(-1);
  const [thMenuConfig, setThMenuConfig] = React.useState<IThMenuConfig<T>>();
  const [lastCheckChangedRowIndex, setLastCheckChangedRowIndex] =
    React.useState<number>();
  const [windowInnerWidth, setWindowInnerWidth] = React.useState<number>(
    window.innerWidth,
  );

  const isDataFunction = data instanceof Function;
  const [sourceData, setSourceData] = React.useState<T[] | null | undefined>(
    isDataFunction ? undefined : data,
  );
  const [originalFilters, setOriginalFilters] =
    React.useState<ITableDataState>();
  const [tableDataState, setTableDataState] = React.useState<ITableDataState>({
    searchQuery: "",
    columnFilterMap: defaultColumnFilters || {},
    sortColumn: defaultSortColumn,
    sortAsc: defaultSortAsc,
  });
  const [locationHash, setLocationHash] =
    React.useState<ITableDataState | null>(
      useFilterStateHash ? parseLocationHash(window.location.hash) : null,
    );

  React.useEffect(() => {
    if (isDataFunction) {
      return;
    }
    setSourceData(data);
  }, [data, isDataFunction]);

  React.useEffect(() => {
    if (!isDataFunction || sourceData !== undefined) {
      return;
    }
    data(
      locationHash && Object.keys(locationHash).length !== 0
        ? {
            ...locationHash,
            columnFilterMap: locationHash.columnFilterMap || {},
          }
        : tableDataState,
    ).then(setSourceData);
  }, [data, isDataFunction, locationHash, sourceData, tableDataState]);

  React.useEffect(() => {
    if (!originalFilters) {
      setOriginalFilters(tableDataState);
    }
  }, [tableDataState, originalFilters]);

  React.useEffect(() => {
    if (!useFilterStateHash) {
      return;
    }
    const onHashChange = () => {
      setLocationHash(parseLocationHash(window.location.hash));
    };
    window.addEventListener("hashchange", onHashChange);
    return () => {
      window.removeEventListener("hashchange", onHashChange);
    };
  }, [useFilterStateHash]);

  React.useEffect(() => {
    if (!useFilterStateHash) {
      return;
    }
    const { columnFilterMap, searchQuery, sortAsc, sortColumn } =
      locationHash || {};
    const newTableState = {
      searchQuery: searchQuery || "",
      columnFilterMap: columnFilterMap || defaultColumnFilters || {},
      sortAsc: sortAsc === undefined ? defaultSortAsc : sortAsc,
      sortColumn: (sortColumn === undefined
        ? defaultSortColumn
        : sortColumn) as string,
    };
    setTableDataState((currentTableState) =>
      isEqual(newTableState, currentTableState)
        ? currentTableState
        : newTableState,
    );
  }, [
    locationHash,
    useFilterStateHash,
    defaultColumnFilters,
    defaultSortAsc,
    defaultSortColumn,
  ]);

  React.useEffect(() => {
    if (onTableDataStateChange) {
      onTableDataStateChange(tableDataState);
    }
  }, [onTableDataStateChange, tableDataState]);

  const updateWindowInnerWidth = React.useCallback(() => {
    setWindowInnerWidth(window.innerWidth);
  }, []);

  React.useEffect(() => {
    if (width) {
      return;
    }
    // no explicit width set? Recalculate width based variableSizeGridRef after resizes!
    window.addEventListener("resize", updateWindowInnerWidth);
    window.addEventListener("focus", updateWindowInnerWidth);
    return () => {
      window.removeEventListener("resize", updateWindowInnerWidth);
      window.removeEventListener("focus", updateWindowInnerWidth);
    };
  }, [updateWindowInnerWidth, width]);

  const [schemaTableRef, setSchemaTableElement] =
    React.useState<HTMLDivElement | null>(null);
  const tableWidth = React.useMemo(
    () => width || schemaTableRef?.getBoundingClientRect().width || 0,
    [schemaTableRef, width],
  );

  const isDirty = React.useMemo(() => {
    return !isEqual(tableDataState, originalFilters) && isDataFunction;
  }, [tableDataState, originalFilters, isDataFunction]);

  // LEAVE THIS
  // Make sure optional props exist, without breaking proper reactivity. E.g. Fix positioning of filter menu
  const { properties, required } = React.useMemo(
    () => ({
      properties: {},
      required: [],
      ...schema,
    }),
    [schema],
  );

  const serializedStoredColumnNames = settingsStorageKey
    ? localStorage.getItem(`${settingsStorageKey}.columnNames`)
    : null;
  const serializedStoredColumnWidths = settingsStorageKey
    ? localStorage.getItem(`${settingsStorageKey}.columnWidths`)
    : null;

  const columnNames = React.useMemo<string[]>(() => {
    if (customColumnNames) {
      return customColumnNames;
    }
    const storedColumnNames = serializedStoredColumnNames
      ? JSON.parse(serializedStoredColumnNames)
      : undefined;
    let freshColumnNames = Object.keys(properties);
    if (setCheckedIndexes) {
      freshColumnNames.unshift(SELECT_ALL_COLUMN_NAME);
    }
    if (config) {
      Object.keys(config).forEach((configKey) => {
        if (!freshColumnNames.includes(configKey)) {
          freshColumnNames.push(configKey);
        }
      });

      const invisibleColumns = Object.entries(config).reduce<string[]>(
        (prev, [propName, propConfig]) => {
          if (propConfig.hidden) {
            prev.push(propName);
          }
          return prev;
        },
        [],
      );

      freshColumnNames = freshColumnNames
        .filter((key) => !invisibleColumns.includes(key))
        .sort((columnA, columnB) => {
          let orderA = config[columnA] ? config[columnA].order : undefined;
          if (orderA === undefined) {
            orderA = Object.keys(properties).findIndex(
              (propName) => propName === columnA,
            );
          }
          let orderB = config[columnB] ? config[columnB].order : undefined;
          if (orderB === undefined) {
            orderB = Object.keys(properties).findIndex(
              (propName) => propName === columnB,
            );
          }
          if (
            columnB === SELECT_ALL_COLUMN_NAME ||
            columnA === SELECT_ALL_COLUMN_NAME
          ) {
            return 0;
          }
          if (orderA === -1) {
            return 1;
          }
          if (orderB === -1) {
            return -1;
          }
          return orderA - orderB;
        });
    }

    return storedColumnNames?.length === freshColumnNames.length
      ? storedColumnNames
      : freshColumnNames;
  }, [
    config,
    customColumnNames,
    setCheckedIndexes,
    properties,
    serializedStoredColumnNames,
  ]);

  const renderData = React.useMemo<IRenderData[] | undefined>(
    () =>
      sourceData && columnNames
        ? sourceData.map(
            (object, rowIndex) =>
              columnNames.reduce(
                (prev: IRenderData, propName) => {
                  const schema = properties[propName] as oas31.SchemaObject;
                  const propConfig = config ? config[propName] : undefined;
                  if (propConfig?.renderData) {
                    prev[propName] = propConfig.renderData(object, rowIndex);
                    return prev;
                  }

                  if (!schema || propName === SELECT_ALL_COLUMN_NAME) {
                    prev[propName] = "";
                    return prev;
                  }

                  const rawValue = object
                    ? (object[propName as keyof T] as unknown)
                    : "";
                  switch (schema.type) {
                    case "array":
                      prev[propName] =
                        (schema.items as oas31.SchemaObject)?.type ===
                          "string" && rawValue
                          ? (rawValue as string[])
                              .map((value) => translate(value))
                              .join(", ")
                          : JSON.stringify(rawValue);
                      return prev;

                    case "boolean":
                      prev[propName] =
                        rawValue === undefined ? "?" : rawValue ? "✓" : "✕";
                      return prev;

                    case "number":
                    case "integer":
                      if (rawValue === undefined) {
                        prev[propName] = "";
                        return prev;
                      }
                      prev[propName] =
                        schema.format === "currency"
                          ? currencyFormatter.format(rawValue as number)
                          : numberFormatter.format(rawValue as number);
                      return prev;

                    // @ts-expect-error investigate
                    case "string":
                      if (schema.format === "date" && rawValue) {
                        prev[propName] = ["2999-12-31", "1970-01-01"].includes(
                          rawValue as string,
                        )
                          ? "-"
                          : localeFormat(
                              new Date(rawValue as string),
                              propConfig?.dateFormat || DEFAULT_DATE_FORMAT,
                            );
                        return prev;
                      }
                      if (schema.format === "date-time" && rawValue) {
                        prev[propName] = displayTimezone
                          ? localeFormatInTimeZone(
                              new Date(rawValue as string),
                              displayTimezone,
                              DEFAULT_DATE_TIME_FORMAT,
                            )
                          : localeFormat(
                              new Date(rawValue as string),
                              propConfig?.dateFormat ||
                                DEFAULT_DATE_TIME_FORMAT,
                            );
                        return prev;
                      }
                      if (schema.enum) {
                        prev[propName] = rawValue
                          ? translate(rawValue as string)
                          : "";
                        return prev;
                      }
                    // fallthrough

                    default:
                      prev[propName] = rawValue ? `${rawValue}` : "";
                      return prev;
                  }
                },
                { _index: rowIndex } as IRenderData,
              ) as unknown as IRenderData,
          )
        : undefined,
    [sourceData, columnNames, properties, config, translate, displayTimezone],
  );

  const columnCount = columnNames ? columnNames.length : 0;
  const { dynamicWidthColumnCount, fixedWidthColumnsWidth } =
    React.useMemo(() => {
      let fixedWidthColumnsWidth = 0;
      let dynamicWidthColumnCount = 0;
      (columnNames || []).forEach((propName) => {
        if (propName === SELECT_ALL_COLUMN_NAME) {
          fixedWidthColumnsWidth += SELECT_ALL_COLUMN_WIDTH;
          return;
        }
        const propConfig = config ? config[propName] : undefined;
        if (propConfig?.width) {
          fixedWidthColumnsWidth += propConfig.width;
        } else {
          dynamicWidthColumnCount += 1;
        }
      }, 0);

      return { dynamicWidthColumnCount, fixedWidthColumnsWidth };
    }, [columnNames, config]);

  const columnWidths = React.useMemo<number[]>(() => {
    if (customColumnWidths) {
      return customColumnWidths;
    }
    const storedColumnWidths = serializedStoredColumnWidths
      ? JSON.parse(serializedStoredColumnWidths)
      : undefined;
    const dynamicColumnWidth = Math.max(
      Math.floor(
        (tableWidth - fixedWidthColumnsWidth) / dynamicWidthColumnCount,
      ),
      MINIMUM_COLUMN_WIDTH,
    );
    let roundingDifference =
      (tableWidth - fixedWidthColumnsWidth) % dynamicWidthColumnCount;
    const freshColumnWidths = columnNames.map((propName) => {
      if (propName === SELECT_ALL_COLUMN_NAME) {
        return SELECT_ALL_COLUMN_WIDTH;
      }
      const propConfig = config ? config[propName] : undefined;
      if (propConfig?.width) {
        return propConfig?.width;
      }
      if (roundingDifference) {
        roundingDifference -= 1;
        return dynamicColumnWidth + 1;
      }
      return dynamicColumnWidth;
    });
    return storedColumnWidths?.length === freshColumnWidths.length
      ? storedColumnWidths
      : freshColumnWidths;
  }, [
    columnNames,
    config,
    customColumnWidths,
    dynamicWidthColumnCount,
    fixedWidthColumnsWidth,
    serializedStoredColumnWidths,
    tableWidth,
  ]);

  const getColumnWidth = React.useCallback(
    (columnIndex: number) => (columnWidths ? columnWidths[columnIndex] : 1),
    [columnWidths],
  );

  const filteredRenderData = React.useMemo(() => {
    if (
      !renderData ||
      (!isColumnFilterable && !isSearchable) ||
      isDataFunction
    ) {
      return renderData;
    }

    return renderData.filter((item) => {
      const lcSearchQuery = tableDataState.searchQuery?.toLowerCase() || "";
      if (
        tableDataState.searchQuery &&
        columnNames &&
        !columnNames.find((columnName) =>
          `${item[columnName]}`.toLowerCase().includes(lcSearchQuery),
        )
      ) {
        return false;
      }
      let result = true;
      if (!tableDataState.columnFilterMap) {
        return false;
      }
      Object.entries(tableDataState.columnFilterMap).forEach(
        ([propName, columnFilterValue]) => {
          if (!result || columnFilterValue === undefined) {
            return;
          }

          const propConfig = config ? config[propName] : undefined;
          if (sourceData && propConfig?.filter) {
            result = propConfig.filter(
              sourceData[item._index],
              columnFilterValue,
            );
            return;
          }

          const propSchema = properties[propName] as
            | oas31.SchemaObject
            | undefined;
          // @ts-expect-error investigate
          const rawValue = sourceData[item._index]?.[propName];
          switch (propSchema?.type) {
            case "boolean":
            case "number":
            case "integer":
              if (columnFilterValue && typeof columnFilterValue === "object") {
                if (
                  (columnFilterValue as INumberColumnFilterValue).filterEmpty &&
                  rawValue === undefined
                ) {
                  result = false;
                }

                for (const operation of Object.keys(
                  ENumberColumnFilterOperation,
                )) {
                  const operationFilterValue = (
                    columnFilterValue as INumberColumnFilterValue
                  )[operation as ENumberColumnFilterOperation];
                  if (result && isFinite(operationFilterValue)) {
                    switch (operation) {
                      case ENumberColumnFilterOperation.EQ:
                        if (rawValue !== operationFilterValue) {
                          result = false;
                        }
                        break;

                      case ENumberColumnFilterOperation.GT:
                        if (rawValue <= operationFilterValue) {
                          result = false;
                        }
                        break;

                      case ENumberColumnFilterOperation.LT:
                        if (rawValue >= operationFilterValue) {
                          result = false;
                        }
                        break;
                    }
                  }
                }
              } else {
                if (rawValue === undefined && columnFilterValue === null) {
                  return true;
                }
                result = rawValue === columnFilterValue;
              }
              break;

            // @ts-expect-error investigate
            case "string":
              if (
                typeof columnFilterValue === "object" &&
                (propSchema.format === "date" ||
                  propSchema.format === "date-time")
              ) {
                const { from, to, filterEmpty } =
                  columnFilterValue as IDateColumnFilterValue;
                if (!rawValue) {
                  result = !filterEmpty;
                } else {
                  const rawDate = rawValue ? new Date(rawValue) : undefined;
                  if (
                    filterEmpty === false ||
                    (from && (!rawDate || rawDate < from)) ||
                    (to && (!rawDate || rawDate > to))
                  ) {
                    result = false;
                  }
                }
                return;
              }
            // fall through
            default:
              // fallback by looking at the render value
              if (propSchema?.enum) {
                result = rawValue === columnFilterValue;
                return;
              }

              result = `${item[propName]}`
                .toLowerCase()
                .includes(`${columnFilterValue}`.toLowerCase());
          }
        },
      );
      return result;
    });
  }, [
    renderData,
    isColumnFilterable,
    isSearchable,
    isDataFunction,
    tableDataState.searchQuery,
    tableDataState.columnFilterMap,
    columnNames,
    config,
    sourceData,
    properties,
  ]);

  // Sort the filtered data
  const sortedRenderData = React.useMemo(() => {
    const { sortAsc, sortColumn } = tableDataState;
    if (!sortColumn || !filteredRenderData || !sourceData || isDataFunction) {
      return filteredRenderData;
    }
    const sortSchema = properties[sortColumn as string] as
      | oas31.SchemaObject
      | undefined;

    const propConfig = config ? config[sortColumn] : undefined;
    const columnSort = propConfig?.sort;
    if (columnSort) {
      return filteredRenderData.sort((a, b) => {
        const aData = sourceData[a._index];
        const bData = sourceData[b._index];
        if (!aData) {
          return 1;
        }
        if (!bData) {
          return -1;
        }
        return columnSort(
          aData,
          bData,
          sortAsc === undefined ? defaultSortAsc : sortAsc,
        );
      });
    }

    const isDate = sortSchema && sortSchema.format?.startsWith("date");
    const sortByValue = getSortByValue<T>(sortSchema, propConfig);
    return filteredRenderData.sort((a, b) => {
      let x: string | T[keyof T] | Date =
        sortByValue && sourceData[a._index]
          ? sourceData[a._index][sortColumn as keyof T]
          : a[sortColumn as string].toLowerCase();
      let y: string | T[keyof T] | Date =
        sortByValue && sourceData[b._index]
          ? sourceData[b._index][sortColumn as keyof T]
          : b[sortColumn as string].toLowerCase();
      if (sortByValue && isDate) {
        x = new Date(x as string);
        if (isNaN(x.getTime())) {
          x = startOfTheWorldDate;
        }
        y = new Date(y as string);
        if (isNaN(y.getTime())) {
          y = startOfTheWorldDate;
        }
      }

      if (x === y) {
        return 0;
      }
      if (!x) {
        return sortAsc ? -1 : 1;
      }
      if (!y) {
        return sortAsc ? 1 : -1;
      }
      return (x < y ? 1 : -1) * (sortAsc ? -1 : 1);
    });
  }, [
    config,
    defaultSortAsc,
    filteredRenderData,
    isDataFunction,
    properties,
    sourceData,
    tableDataState,
  ]);

  const disableColumnFilter = React.useCallback(
    (propName: string) => {
      const newColumnFilterMap = { ...tableDataState.columnFilterMap };
      delete newColumnFilterMap[propName];
      setTableDataState({
        ...tableDataState,
        columnFilterMap: newColumnFilterMap,
      });
    },
    [tableDataState],
  );

  const onFilterSortColumn = React.useCallback(
    (sortColumn: string, sortAsc: boolean) => {
      if (useFilterStateHash) {
        window.location.hash = serializeLocationHash({
          ...locationHash,
          sortColumn,
          sortAsc,
        });
        return;
      }
      setTableDataState({ ...tableDataState, sortColumn, sortAsc });
    },
    [tableDataState, locationHash, useFilterStateHash],
  );

  const onSearchChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setTableDataState({
        ...tableDataState,
        searchQuery: e.currentTarget.value,
      });
    },
    [tableDataState],
  );

  const refreshData = React.useCallback(() => {
    setOriginalFilters(tableDataState);
    setSourceData(undefined);
  }, [tableDataState]);

  React.useEffect(() => {
    if (
      !autoRender ||
      !locationHash ||
      !originalFilters ||
      !useFilterStateHash
    ) {
      return;
    }
    const hashKeys = Object.keys(locationHash);
    let shouldTriggerRefresh: boolean = false;
    for (const key of hashKeys) {
      const currentLocationValue = locationHash[key as keyof ITableDataState];
      const currentOriginalFilterValue =
        originalFilters[key as keyof ITableDataState];

      if (
        typeof currentLocationValue === "string" &&
        currentLocationValue !== currentOriginalFilterValue
      ) {
        shouldTriggerRefresh = true;
        break;
      }

      if (
        typeof currentLocationValue === "object" &&
        !isEqual(currentLocationValue, currentOriginalFilterValue)
      ) {
        shouldTriggerRefresh = true;
        break;
      }
    }

    if (!shouldTriggerRefresh) {
      return;
    }
    refreshData();
  }, [
    autoRender,
    locationHash,
    originalFilters,
    refreshData,
    useFilterStateHash,
  ]);

  const onInputKeyDown = React.useCallback(
    (e: React.KeyboardEvent<HTMLElement>) => {
      if (e.key === "Enter") {
        setThMenuConfig(undefined);
        if (isDirty) {
          refreshData();
        }
        if (
          onSearchEnter &&
          e.currentTarget.className === "mig-schema-table__search"
        ) {
          onSearchEnter(tableDataState.searchQuery || "");
        }
      }
    },
    [isDirty, tableDataState.searchQuery, onSearchEnter, refreshData],
  );

  const getRowHeight = React.useCallback(() => rowHeight, [rowHeight]);
  const rowWidth = React.useMemo(() => sum(columnWidths), [columnWidths]);
  const rowCount = React.useMemo(
    () => (sortedRenderData ? sortedRenderData.length : 0),
    [sortedRenderData],
  );

  const tableBodyHeight = React.useMemo(() => {
    const rowsHeight = rowHeight * rowCount;
    const rowsMaxHeight = maxHeight ? maxHeight - (isSearchable ? 50 : 0) : 0;
    return rowsMaxHeight && rowsMaxHeight < rowsHeight
      ? rowsMaxHeight
      : rowsHeight;
  }, [maxHeight, isSearchable, rowCount, rowHeight]);

  const onPopoverClose = React.useCallback(
    (e: MouseEvent | React.MouseEvent) => {
      setThMenuConfig(undefined);
      e.preventDefault();
      e.stopPropagation();
    },
    [],
  );

  const onSchemaColumnFilterChange = React.useCallback(
    (newColumnFilterValue: T | undefined, persistState?: boolean) => {
      if (!thMenuConfig) {
        return;
      }

      if (useFilterStateHash && persistState !== false) {
        window.location.hash = serializeLocationHash({
          ...locationHash,
          columnFilterMap: {
            ...tableDataState.columnFilterMap,
            [thMenuConfig.propName]: newColumnFilterValue,
          },
        });
        return;
      }

      if (newColumnFilterValue === undefined) {
        disableColumnFilter(thMenuConfig.propName);
        return;
      }
      const newColumnFilterMap = {
        ...tableDataState.columnFilterMap,
        [thMenuConfig.propName]: newColumnFilterValue,
      };
      setTableDataState({
        ...tableDataState,
        columnFilterMap: newColumnFilterMap,
      });
    },
    [
      disableColumnFilter,
      tableDataState,
      locationHash,
      thMenuConfig,
      useFilterStateHash,
    ],
  );

  const onClearFiltersButtonClick = React.useCallback(() => {
    const defaultValue = {
      searchQuery: "",
      columnFilterMap: {}, // Clear button should clear _ALL_ filters, do _NOT_ revert to defaultColumnFilters
      sortColumn: defaultSortColumn,
      sortAsc: defaultSortAsc,
    };
    if (useFilterStateHash) {
      window.location.hash = serializeLocationHash(defaultValue);
    }
    setOriginalFilters(defaultValue);
    setTableDataState(defaultValue);
  }, [useFilterStateHash, defaultSortAsc, defaultSortColumn]);

  const onSearchBlur = React.useCallback(() => {
    const oldSearchQuery = locationHash?.searchQuery || "";
    if (
      useFilterStateHash &&
      // prevent hash change for undefined vs empty string compare
      (tableDataState.searchQuery || oldSearchQuery) &&
      tableDataState.searchQuery !== oldSearchQuery
    ) {
      window.location.hash = serializeLocationHash({
        ...locationHash,
        searchQuery: tableDataState.searchQuery,
      });
    }
  }, [locationHash, tableDataState.searchQuery, useFilterStateHash]);

  const onExportDataClick = React.useCallback(
    async (e: React.MouseEvent) => {
      e.preventDefault();
      e.stopPropagation();
      if (!sortedRenderData) {
        return;
      }
      const { renderDataToExcel } = await import(`./renderDataToExcel`);
      renderDataToExcel(sortedRenderData);
    },
    [sortedRenderData],
  );

  const onClearSettingsButtonClick = React.useCallback(() => {
    if (!settingsStorageKey) {
      return;
    }
    localStorage.removeItem(`${settingsStorageKey}.columnNames`);
    localStorage.removeItem(`${settingsStorageKey}.columnWidths`);
    setCustomColumnNames(undefined);
    setCustomColumnWidths(undefined);
  }, [settingsStorageKey]);

  const isItemLoaded = React.useCallback(
    (rowIndex: number) => {
      if (!sortedRenderData || !sourceData || !loadMoreItems || !itemCount) {
        return true;
      }
      return sourceData[sortedRenderData[rowIndex]._index] !== undefined;
    },
    [itemCount, loadMoreItems, sortedRenderData, sourceData],
  );

  const TableBody = React.useMemo(() => {
    const shouldRenderTable = sourceData && !isDirty;
    if (!shouldRenderTable && !loadMoreItems) {
      return (
        <div
          style={{
            width: rowWidth,
            height: Math.max(50, tableBodyHeight),
            border: "1px solid #BBB",
            textAlign: "center",
            display: "flex",
            backgroundColor: "#CCC",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {isDirty ? (
            <button onClick={refreshData} className="btn border">
              Refresh data
            </button>
          ) : (
            <div>⌛ Loading...</div>
          )}
        </div>
      );
    }

    if (loadMoreItems && itemCount) {
      return (
        <InfiniteLoader
          ref={infiniteLoaderRef}
          isItemLoaded={isItemLoaded}
          itemCount={itemCount}
          loadMoreItems={loadMoreItems}
        >
          {({ onItemsRendered, ref }) => (
            <VariableSizeGrid
              className="mig-schema-table__tbody"
              key={`tbody_${tableBodyHeight}_${rowWidth}_${tableDataState.sortColumn}_${tableDataState.sortAsc}_${tableDataState.searchQuery}_${columnCount}_${columnWidths.join(
                " ",
              )}`}
              estimatedRowHeight={rowHeight}
              height={tableBodyHeight}
              width={rowWidth}
              columnWidth={getColumnWidth}
              rowHeight={getRowHeight}
              columnCount={columnCount}
              rowCount={rowCount}
              ref={ref}
              onItemsRendered={({
                overscanRowStartIndex,
                overscanRowStopIndex,
                visibleRowStartIndex,
                visibleRowStopIndex,
              }) =>
                onItemsRendered({
                  overscanStartIndex: overscanRowStartIndex,
                  overscanStopIndex: overscanRowStopIndex,
                  visibleStartIndex: visibleRowStartIndex,
                  visibleStopIndex: visibleRowStopIndex,
                })
              }
            >
              {Td}
            </VariableSizeGrid>
          )}
        </InfiniteLoader>
      );
    }

    return (
      <VariableSizeGrid
        className="mig-schema-table__tbody"
        key={`tbody_${tableBodyHeight}_${rowWidth}_${tableDataState.sortColumn}_${tableDataState.sortAsc}_${tableDataState.searchQuery}_${columnCount}_${columnWidths.join(
          " ",
        )}`}
        estimatedRowHeight={rowHeight}
        height={tableBodyHeight}
        width={rowWidth}
        columnWidth={getColumnWidth}
        rowHeight={getRowHeight}
        columnCount={columnCount}
        rowCount={rowCount}
        ref={variableSizeGridRef}
      >
        {Td}
      </VariableSizeGrid>
    );
  }, [
    columnCount,
    columnWidths,
    getColumnWidth,
    getRowHeight,
    infiniteLoaderRef,
    isDirty,
    isItemLoaded,
    itemCount,
    loadMoreItems,
    refreshData,
    rowCount,
    rowHeight,
    rowWidth,
    tableDataState.searchQuery,
    tableDataState.sortAsc,
    tableDataState.sortColumn,
    sourceData,
    tableBodyHeight,
    variableSizeGridRef,
  ]);

  const tableStyle = React.useMemo(
    () => ({
      ...style,
      // if no width is provided, leave undefined so we can measure the ref after render
      width: width ? Math.min(rowWidth, tableWidth) : undefined,
    }),
    [rowWidth, style, tableWidth, width],
  );

  const tableRef = React.useCallback(
    (newSchemaTable: HTMLDivElement | null) =>
      setSchemaTableElement(newSchemaTable),
    [],
  );

  const SearchInput = CustomSearchInput || "input";

  return (
    <SchemaTableContext.Provider
      value={{
        checkedIndexes,
        columnNames,
        columnWidths,
        config,
        disabledCheckedIndexes,
        displayTimezone,
        dropTargetIndex,
        getRowClassName,
        getRowSelected,
        isColumnFilterable,
        isItemLoaded,
        isSortable,
        lastCheckChangedRowIndex,
        onRowClick,
        onRowDoubleClick,
        properties,
        required,
        setCheckedIndexes,
        setCustomColumnNames,
        setCustomColumnWidths,
        setDropTargetIndex,
        setFilterSortColumn: onFilterSortColumn,
        setLastCheckChangedRowIndex,
        setMenuConfig: setThMenuConfig,
        settingsStorageKey,
        sortedRenderData,
        sourceData,
        tableDataState: tableDataState,
        translate: translate || defaultTranslate,
      }}
    >
      <div
        className={`mig-schema-table${
          onRowClick ? " mig-schema-table--clickable-rows" : ""
        }`}
        style={tableStyle}
        // enforce re-render after window size change to support dynamic width
        key={windowInnerWidth}
        // ref is part of state, to force re-calculation of tableWidth after the ref has been rendered
        ref={tableRef}
        role="table"
      >
        <div className={"mig-schema-table__action-container"}>
          <div>
            {isSearchable ? (
              <SearchInput
                className="mig-schema-table__search"
                type="search"
                name="search"
                autoComplete="off"
                placeholder={searchPlaceholder || translate("search...")}
                value={tableDataState.searchQuery}
                onChange={onSearchChange}
                onKeyDown={onInputKeyDown}
                autoFocus={enableAutoFocus}
                onBlur={onSearchBlur}
              />
            ) : null}
          </div>
          {CustomElement ? (
            <CustomElement
              {...customElementProps}
              renderData={sortedRenderData}
            />
          ) : (
            <div className="mig-schema-table__custom_element_placeholder" />
          )}
          {enableRowCounter && Array.isArray(data) ? (
            <span className="mig-schema-table__row_counter">
              {translate(
                "showingFilteredCountOfTotalCount",
                sortedRenderData?.length || 0,
                data.length,
              )}
            </span>
          ) : null}
          {isExportable ? (
            <button
              onClick={onExportDataClick}
              style={{ marginLeft: 8 }}
              disabled={!sortedRenderData?.length}
            >
              {translate("exportData")}
            </button>
          ) : null}
          {isSearchable || isColumnFilterable ? (
            <button
              onClick={onClearFiltersButtonClick}
              style={{ marginLeft: 8 }}
              disabled={
                tableDataState.columnFilterMap && tableDataState.searchQuery
                  ? Object.keys(tableDataState.columnFilterMap).length +
                      tableDataState.searchQuery.length ===
                    0
                  : false
              }
            >
              {translate("clearFilters")}
            </button>
          ) : null}
          {settingsStorageKey ? (
            <button
              onClick={onClearSettingsButtonClick}
              style={{ marginLeft: 8 }}
              disabled={
                !serializedStoredColumnNames && !serializedStoredColumnWidths
              }
            >
              {translate("clearSettings")}
            </button>
          ) : null}
        </div>
        <div className="mig-schema-table__column_resize_container">
          <Heading
            key={`thead_${rowWidth}_${tableDataState.sortColumn}_${tableDataState.sortAsc}_${tableDataState.searchQuery}_${columnWidths.join(
              " ",
            )}`}
            height={50}
            itemCount={columnCount}
            itemSize={getColumnWidth}
            layout="horizontal"
            width={rowWidth}
            sortAsc={tableDataState.sortAsc}
            setFilterSortColumn={onFilterSortColumn}
            sortColumn={tableDataState.sortColumn}
            sortedRenderData={sortedRenderData}
            className="mig-schema-table__th-row"
          >
            {Th}
          </Heading>
          {TableBody}
          {isResizable ? (
            <ColumnResizers
              columnWidths={columnWidths}
              resizeColumnIndex={resizeColumnIndex}
              setResizeColumnIndex={setResizeColumnIndex}
              tableBodyHeight={tableBodyHeight}
            />
          ) : null}
        </div>
        {sourceData ? (
          sourceData.length > 0 ? (
            <>
              {createPortal(
                thMenuConfig ? (
                  <ThMenu
                    isFilterable={
                      isColumnFilterable &&
                      thMenuConfig.propConfig?.isFilterable !== false
                    }
                    isSortable={getIsColumnSortable(
                      !!isSortable,
                      schema.properties![
                        thMenuConfig.propName
                      ] as oas31.SchemaObject,
                      thMenuConfig.propConfig,
                    )}
                    onChange={onSchemaColumnFilterChange}
                    onClose={onPopoverClose}
                    onInputKeyDown={onInputKeyDown}
                    propConfig={thMenuConfig.propConfig}
                    propIsRequired={thMenuConfig.propIsRequired}
                    propName={thMenuConfig.propName}
                    propSchema={
                      schema.properties![
                        thMenuConfig.propName
                      ] as oas31.SchemaObject
                    }
                    referenceElement={thMenuConfig.referenceElement}
                    setFilterSortColumn={onFilterSortColumn}
                    translate={translate}
                    value={
                      tableDataState.columnFilterMap
                        ? (tableDataState.columnFilterMap[
                            thMenuConfig.propName
                          ] as T)
                        : null
                    }
                  />
                ) : null,
                document.body,
              )}
            </>
          ) : (
            <div className={"mig-schema-table__no_data"}>No data found</div>
          )
        ) : null}
      </div>
    </SchemaTableContext.Provider>
  );
};

const MemoizedSchemaTable = React.memo(SchemaTable) as typeof SchemaTable;

export default MemoizedSchemaTable;
