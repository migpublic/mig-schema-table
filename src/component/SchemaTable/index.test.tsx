import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import SchemaTable from "./index";
import exampleData, { exampleDataSchema } from "../../exampleData";

it("renders an empty table", () => {
  render(<SchemaTable data={[]} schema={{}} />);
  const table = screen.getByRole("table");
  expect(table).toBeInTheDocument();
});

it("should match the configured width without configuration", () => {
  render(
    <SchemaTable data={exampleData} schema={exampleDataSchema} width={1000} />,
  );
  const table = screen.getByRole("table");
  const renderedWidth = [
    ...table.querySelectorAll(".mig-schema-table__th"),
  ].reduce(
    (prev: number, node) =>
      prev + parseInt((node as HTMLDivElement).style.width),
    0,
  );
  expect(renderedWidth).toBe(1000);
});
