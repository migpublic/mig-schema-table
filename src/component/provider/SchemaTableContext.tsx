import * as React from "react";
import { IColumnConfig, IRenderData } from "../types.ts";
import { ReferenceObject, SchemaObject } from "openapi3-ts/oas31";
import { ITableDataState } from "../SchemaTable";
import { Dispatch, SetStateAction } from "react";
import { IThMenuConfig } from "../SchemaTable/ThMenu";
import { emptyFn } from "mig-data-tools";

//eslint-disable-next-line
export interface ISchemaTableContext<T = any> {
  checkedIndexes?: number[];
  columnNames: string[];
  columnWidths: number[];
  config?: { [propName: string]: IColumnConfig<T> };
  disabledCheckedIndexes?: number[];
  dropTargetIndex: number;
  getRowClassName?: (
    rowData: T,
    dataIndex: number,
    filteredSortedData: IRenderData[],
  ) => string;
  getRowSelected?: (rowData: T, dataIndex: number) => boolean;
  isColumnFilterable: boolean;
  isItemLoaded: (rowIndex: number) => boolean;
  isSortable: boolean;
  setCheckedIndexes?: Dispatch<SetStateAction<number[]>>;
  onRowClick?: (rowData: T, dataIndex: number, event: React.MouseEvent) => void;
  onRowDoubleClick?: (
    rowData: T,
    dataIndex: number,
    event: React.MouseEvent,
  ) => void;
  lastCheckChangedRowIndex?: number;
  setLastCheckChangedRowIndex: Dispatch<SetStateAction<number | undefined>>;
  properties: { [propertyName: string]: SchemaObject | ReferenceObject };
  required: string[];
  setCustomColumnNames: Dispatch<SetStateAction<string[] | undefined>>;
  setCustomColumnWidths: Dispatch<SetStateAction<number[] | undefined>>;
  setMenuConfig: React.Dispatch<
    React.SetStateAction<IThMenuConfig<T> | undefined>
  >;
  setDropTargetIndex: (dropTargetIndex: number) => void;
  setFilterSortColumn: (sortColumn: string, sortAsc: boolean) => void;
  settingsStorageKey?: string;
  tableDataState: ITableDataState;
  sortedRenderData?: IRenderData[];
  sourceData?: T[] | null;
  translate: (key: string, ...args: Array<string | number>) => string;
  displayTimezone?: string;
}

export const SchemaTableContext = React.createContext<ISchemaTableContext>({
  checkedIndexes: [],
  columnNames: [],
  columnWidths: [],
  config: {},
  disabledCheckedIndexes: [],
  dropTargetIndex: -1,
  isColumnFilterable: false,
  isItemLoaded: () => false,
  isSortable: false,
  properties: {},
  required: [],
  setCustomColumnNames: emptyFn,
  setCustomColumnWidths: emptyFn,
  setDropTargetIndex: emptyFn,
  setFilterSortColumn: emptyFn,
  setLastCheckChangedRowIndex: emptyFn,
  setMenuConfig: emptyFn,
  tableDataState: {
    searchQuery: "",
    columnFilterMap: {},
    sortColumn: "",
    sortAsc: false,
  },
  translate: emptyFn as () => string,
});
