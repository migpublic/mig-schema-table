import { oas31 } from "openapi3-ts";

export const userSchema: oas31.SchemaObject = {
  type: "object",
  properties: {
    id: {
      type: "number",
    },
    username: {
      type: "string",
    },
    displayName: {
      type: "string",
    },
    dob: {
      type: "string",
      format: "date",
    },
    email: {
      type: "string",
    },
    mobileNumber: {
      type: "string",
    },
    status: {
      type: "string",
      enum: ["active", "not-active"],
    },
  },
  required: [
    "id",
    "username",
    "displayName",
    "dob",
    "email",
    "mobileNumber",
    "status",
  ],
};

export interface IUser {
  id: number;
  username: string;
  displayName: string;
  /** Format: date */
  dob: string;
  email: string;
  mobileNumber: string;
  status: "active" | "not-active";
}

export const users: IUser[] = [
  {
    id: 1,
    username: "ariyadhana",
    dob: "1996-10-15",
    displayName: "Ari899",
    email: "ari.mig@gmail.com",
    status: "active",
    mobileNumber: "081281208299",
  },
];
