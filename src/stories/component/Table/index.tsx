import MigSchemaTable from "../../../component/SchemaTable";
import "../../../component/index.scss";
import { IUser } from "../../MockData/User";
import { oas31 } from "openapi3-ts";

const SchemaTable = ({
  schema,
  data,
  width,
  isExportable,
  isResizable,
  isSortable,
  isSearchable,
  isColumnFilterable,
  enableRowCounter,
}: {
  schema: oas31.SchemaObject;
  data: IUser[];
  width: number;
  isExportable?: boolean;
  isResizable?: boolean;
  isSortable?: boolean;
  isSearchable?: boolean;
  isColumnFilterable?: boolean;
  enableRowCounter?: boolean;
}) => {
  return (
    <MigSchemaTable<IUser>
      schema={schema}
      data={data || []}
      width={width}
      isExportable={isExportable}
      isResizable={isResizable}
      isSortable={isSortable}
      isSearchable={isSearchable}
      isColumnFilterable={isColumnFilterable}
      enableRowCounter={enableRowCounter}
    />
  );
};

export default SchemaTable;
