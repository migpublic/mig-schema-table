import type { Meta, StoryObj } from "@storybook/react";
import SchemaTable from "../Table";
import { users, userSchema } from "../../MockData/User";

const meta: Meta<typeof SchemaTable> = {
  component: SchemaTable,
  title: "Async Table",
  tags: ["autodocs"],
  parameters: {
    docs: {
      code: '<Button variant="primary">Click me</Button>',
      description: {
        component:
          "Mig schema table is able to receive data as a normal array or with the asynchronous data.",
      },
    },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const AsyncTable: Story = {
  args: {
    schema: userSchema,
    width: 940,
    data: users,
    isExportable: true,
    isColumnFilterable: true,
    isResizable: true,
    isSearchable: true,
    isSortable: true,
    enableRowCounter: true,
  },
};
