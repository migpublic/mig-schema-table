import MigSchemaTable from "../../../component/SchemaTable";
import "../../../component/index.scss";
import { IUser } from "../../MockData/User";
import { oas31 } from "openapi3-ts";

const SchemaTable = (props: {
  schema: oas31.SchemaObject;
  data: IUser[];
  width: number;
}) => {
  const { schema, data, width } = props;
  return (
    <MigSchemaTable<IUser> schema={schema} data={data || []} width={width} />
  );
};

export default SchemaTable;
