import type { Meta, StoryObj } from "@storybook/react";
import SchemaTable from "../Table";
import { users, userSchema } from "../../MockData/User";

const meta: Meta<typeof SchemaTable> = {
  component: SchemaTable,
  title: "Basic Table",
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component:
          "Basic table required minimum props in order to render the table correctly. Schema, width, and data. " +
          "Some functionalities are active by default (enableRowCounter,resizeable column,exportable table data,filterable column,sortable column, and search functionality)",
      },
    },
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const BasicTable: Story = {
  args: {
    schema: userSchema,
    width: 940,
    data: users,
    isExportable: true,
    isColumnFilterable: true,
    isResizable: true,
    isSearchable: true,
    isSortable: true,
    enableRowCounter: true,
  },
};
