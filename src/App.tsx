import React from "react";
import { VariableSizeGrid } from "react-window";
import InfiniteLoader from "react-window-infinite-loader";
import { SchemaObject } from "openapi3-ts/oas31";

import {
  IColumnConfig,
  IFilterMenuComponentProps,
  IRenderData,
  SchemaTable,
} from "./component";
import exampleData, { exampleDataSchema, IExampleData } from "./exampleData";
import "./App.scss";

export type { IColumnConfig, IRenderData };
export { SchemaTable };

const App = () => {
  const [checkedIndexes, setCheckedIndexes] = React.useState<number[]>([]);
  const variableSizeGridRef = React.useRef<VariableSizeGrid>(null);
  const [data /*, _setData*/] = React.useState<IExampleData[]>([
    ...exampleData,
    // ...Array.from({ length: 200 }).map(() => undefined),
  ] as IExampleData[]);
  const [tableKey, setTableKey] = React.useState<string>();

  // const loadMoreItems = React.useCallback(
  //   async (startIndex: number, stopIndex: number) => {
  //     await new Promise((resolve) => {
  //       const newData: IExampleData[] = [...data];
  //       setTimeout(() => {
  //         for (let index = startIndex; index <= stopIndex; index++) {
  //           newData[index] = {
  //             jobId: `Job-${index}`,
  //             jobType: "clean",
  //             topLevelId: 1234,
  //             hostId: "1",
  //             sourceId: "1",
  //             sources: ["fromIndexPage"],
  //             priority: 1,
  //             insertDateTime: "2023-06-01 11:15",
  //             scrapeStartDateTime: "2023-06-01 01:00",
  //             scrapeEndDateTime: "2023-06-01 01:10",
  //             scraperHost: "scraperHost-1",
  //             scraperContainer: "scraperContainer-1",
  //             errorMessage: "Failed",
  //             url: `www.job${index}.com`,
  //             price: 123.45,
  //             retryCount: 1,
  //             isError: false,
  //             taskType: "ttNonArticle",
  //           };
  //         }
  //         setData(newData);
  //         resolve(true);
  //       }, 2500);
  //     });
  //   },
  //   [data],
  // );

  // In some situations (e.g. Mediaweb) a re-render is required when items are selected
  const infiniteLoaderRef = React.useRef<InfiniteLoader>(null);
  React.useEffect(() => {
    if (infiniteLoaderRef.current) {
      console.log("exec resetloadMoreItemsCache!", infiniteLoaderRef.current);
      infiniteLoaderRef.current.resetloadMoreItemsCache();
    }
  }, [checkedIndexes]);

  const getData = React.useCallback(async (): Promise<IExampleData[]> => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(data);
      }, 1500);
    });
  }, [data]);

  return (
    <div
      style={{
        margin: 0,
        padding: 0,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <div>
        <button
          onClick={() => {
            setTableKey(new Date().toISOString());
          }}
        >
          SET NEW TABLE KEY
        </button>
      </div>
      <div key={`table-0`} style={{ overflow: "auto" }}>
        <SchemaTable<IExampleData>
          key={tableKey}
          checkedIndexes={checkedIndexes}
          data={getData}
          disabledCheckedIndexes={[0]}
          enableAutoFocus={false}
          config={{
            jobType: {
              hoverTitle: "This is the job type",
              filter: (rowData, columnFilterValue) => {
                console.log(columnFilterValue);
                return ((columnFilterValue || []) as string[]).includes(
                  rowData.jobType,
                );
              },
              FilterMenu: ({
                columnFilterValue,
                onChange,
              }: IFilterMenuComponentProps<string[]>) => {
                return (
                  <select
                    multiple
                    onChange={(e) =>
                      onChange(
                        [...e.target.selectedOptions].map(
                          (option) => option.value,
                        ),
                        false,
                      )
                    }
                    onBlur={() => {
                      onChange(columnFilterValue, true);
                    }}
                    value={(columnFilterValue || []) as string[]}
                  >
                    {(
                      exampleDataSchema.properties!.jobType as SchemaObject
                    ).enum!.map((value: string) => (
                      <option value={value} key={value}>
                        {value}
                      </option>
                    ))}
                  </select>
                );
              },
            },
          }}
          infiniteLoaderRef={infiniteLoaderRef}
          isSearchable
          isSortable
          // itemCount={data.length}
          // loadMoreItems={loadMoreItems}
          maxHeight={500}
          setCheckedIndexes={setCheckedIndexes}
          onSearchEnter={console.log}
          onRowClick={console.log}
          schema={exampleDataSchema}
          variableSizeGridRef={variableSizeGridRef}
          useFilterStateHash
          autoRender={true}
          defaultSortColumn={"domainId"}
          displayTimezone={"Asia/Jakarta"}
        />
      </div>
    </div>
  );
};

export default App;
