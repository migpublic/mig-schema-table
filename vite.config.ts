import { defineConfig } from "vite";
import { resolve } from "node:path";
import react from "@vitejs/plugin-react-swc";
import * as packageJson from "./package.json";
import dts from "vite-plugin-dts";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    dts({
      include: ["src/component/**"],
    }),
  ],
  build: {
    lib: {
      entry: resolve("src", "component/index.ts"),
      name: "mig-schema-table",
      formats: ["es", "umd"],
      fileName: (format) => `mig-schema-table.${format}.js`,
    },
    rollupOptions: {
      external: [...Object.keys(packageJson.peerDependencies)],
      output: {
        globals: {
          react: "React",
          "react-dom": "ReactDOM",
        },
      },
    },
  },
});
